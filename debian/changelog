fpconst (0.7.2-7) UNRELEASED; urgency=medium

  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <novy@ondrej.org>  Tue, 29 Mar 2016 21:37:32 +0200

fpconst (0.7.2-6) unstable; urgency=medium

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.
  * Drop obsolete Conflicts with python2.3-soappy and python2.4-soappy.

  [ Andrey Rahmatullin ]
  * Port from python-support to dh-python (Closes: #786262).

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Mon, 24 Aug 2015 23:26:34 +0500

fpconst (0.7.2-5) unstable; urgency=low

  [ Bernd Zeimetz ]
  * debian/control:
    - Adding Homepage: field
    - Rename XS-Vcs-* fields to Vcs-* (dpkg supports them now)
    - Updating my email address.

  [ Sandro Tosi ]
  * debian/watch
    - updated to new pypi simplier URL
  * debian/control
    - added op=log to Vcs-Browser
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Jakub Wilk ]
  * debian/copyright:
    - Stop including the complete text of the Apache 2.0 license.

  [ Stefano Zacchiroli ]
  * debian/control
    - remove myself from Uploaders
    - update Vcs-Browser to point to anonscm.debian.org

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 03 Feb 2012 14:54:49 +0100

fpconst (0.7.2-4) unstable; urgency=low

  * debian/watch:
    - fixing watch file to reflect the new URL in Python's cheeseshop

 -- Bernd Zeimetz <bernd@bzed.de>  Wed, 01 Aug 2007 23:54:04 +0200

fpconst (0.7.2-3) unstable; urgency=low

  * debian/control
    - avoid debian version in Conflicts with older python-soappy (make
      backporter life easier)
    - added myself as an uploader

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 26 Apr 2007 12:13:59 +0200

fpconst (0.7.2-2) unstable; urgency=low

  * debian/control
    - Changing wrong Architecture: any to all
  * Uploading to unstable

 -- Bernd Zeimetz <bernd@bzed.de>  Sun,  1 Apr 2007 19:30:51 +0200

fpconst (0.7.2-1) experimental; urgency=low

  * Initial release (Closes: #415844)

 -- Bernd Zeimetz <bernd@bzed.de>  Sat, 24 Mar 2007 00:14:22 +0100
